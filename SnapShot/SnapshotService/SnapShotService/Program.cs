﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


    // <applicationSettings>
    //    <SnapshotService.SnapshotService>
    //        <setting name="snapshotFilePath" serializeAs="String">
    //            <value>C:\WebCam\snapshots\snapshot.jpg</value>
    //        </setting>
    //        <setting name="snapShotIntervalSeconds" serializeAs="String">
    //            <value>60</value>
    //        </setting>
    //        <setting name="debug" serializeAs="String">
    //            <value>True</value>
    //        </setting>
    //    </SnapshotService.SnapshotService>
    //</applicationSettings>* 

namespace SnapshotService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };

            bool debug = false;
            if (debug == true)
            {
                Service1 srvc = ServicesToRun[0] as Service1;
                srvc.SnapPictures();
                for (;;)
                    Thread.Sleep(1000);
            }
            else
            {
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
