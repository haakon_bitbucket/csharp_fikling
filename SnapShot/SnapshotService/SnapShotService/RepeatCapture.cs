﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace SnapShotService
{
    class RepeatCapture
    {
        private void saveSnapshot(Bitmap snapshot)
        {
            using (System.IO.FileStream snapStream = new System.IO.FileStream(SnapShotService.Default.snapshotFilePath, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))
            {
                snapshot.Save(snapStream, ImageFormat.Jpeg);
            }
        }

        private void Closedown()
        {
            snapshotTimer.Stop();

            cam.Dispose();

            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            }
        }
    }
}
