﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace SnapshotService
{
    public partial class Service1 : ServiceBase
    {
        Capture capturer;
        System.Timers.Timer snapshotTimer = new System.Timers.Timer();
        IntPtr m_ip = IntPtr.Zero;


        public Service1()
        {
            InitializeComponent();
            this.ServiceName = "SnapshotService";
        }

        public void SnapPictures()
        {
            const int VIDEODEVICE = 0; // zero based index of video capture device to use
            const int VIDEOWIDTH = 640; // Depends on video device caps
            const int VIDEOHEIGHT = 480; // Depends on video device caps
            const int VIDEOBITSPERPIXEL = 24; // BitsPerPixel values determined by device

            capturer = new Capture(VIDEODEVICE, VIDEOWIDTH, VIDEOHEIGHT, VIDEOBITSPERPIXEL, null);

            snapshotTimer.Elapsed += new System.Timers.ElapsedEventHandler(TimerEventProcessor);

            // Sets the timer interval to ? seconds.
            //TODO: settings
            int snapshotIntervalSeconds = 60;
            snapshotTimer.Interval = snapshotIntervalSeconds * 1000;
            snapshotTimer.Start();
        }

        private void saveSnapshot(System.Drawing.Bitmap snapshot)
        {
            try
            {
                //string filePath = @"F:\git_rep\csharp-fikling\Snapshot\snapshot.jpg";
                //TODO: settings
                string snapshotFilePath = @"C:\inetpub\WebCam\snapshots\snapshot.jpg";
                using (System.IO.FileStream snapStream = new System.IO.FileStream(snapshotFilePath, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))
                {
                    snapshot.Save(snapStream, ImageFormat.Jpeg);
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Message);
            }
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("SnapshotService started");
            SnapPictures();
        }

        private void TimerEventProcessor(Object sender, System.Timers.ElapsedEventArgs eventArgs)
        {
            snapshotTimer.Stop();
            var snap = captureSnapshot();
            saveSnapshot(snap);
            snapshotTimer.Enabled = true;
        }

        private System.Drawing.Bitmap captureSnapshot()
        {
            // Release any previous buffer
            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            }

            // capture image
            m_ip = capturer.Click();

            System.Drawing.Bitmap b = new System.Drawing.Bitmap(capturer.Width, capturer.Height, capturer.Stride, PixelFormat.Format24bppRgb, m_ip);
            // If the image is upsidedown
            b.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipY);
            //pictureBox1.Image = b;

            return b;
        }

        protected override void OnStop()
        {
            snapshotTimer.Stop();

            capturer.Dispose();

            if (m_ip != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem(m_ip);
                m_ip = IntPtr.Zero;
            }
        }
    }
}
