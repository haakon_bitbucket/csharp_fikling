﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace attributter.Controllers
{
    [AttributeUsage(AttributeTargets.All)]
    public class HelpAttribute : System.Attribute
    {
        public readonly string Url;

        public string Topic
        {
            get 
            {
                return Topic;
            }

            set
            {
                Topic = value;
            }
        }
        public HelpAttribute(string url)
        {
            this.Url = url;
        }

        private string topic;
    }

}