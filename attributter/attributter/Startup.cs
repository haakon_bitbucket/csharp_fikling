﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(attributter.Startup))]
namespace attributter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
