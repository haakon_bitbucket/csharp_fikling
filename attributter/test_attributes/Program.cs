﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;

namespace test_attributes
{
    public class IsTestedAttribute : Attribute
    {
        public override string ToString()
        {
            return "Is Tested";
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class AuthorAttribute : Attribute
    {
        public AuthorAttribute(string name)
        {
            this.name = name;
            this.version = 0;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public int Version
        {
            get
            {
                return version;
            }

            set
            {
                version = value;
            }
        }

        public override string ToString()
        {
            string value = "Author : " + name;
            if (version != 0)
            {
                value += " Version : " + version.ToString();
            }
            return value;
        }

        private string name;
        private int version;
    }

    [Author("Joe Programmer")]
    class Account
    {
        [IsTested]
        public void AddOrder(Order orderToAdd)
        {
            orders.Add(orderToAdd);
        }

        [IsTested]
        public void DeleteOrder(Order orderToDelete)
        {
        }

        public void ChangeOrder(Order orderToChange)
        {

        }

        private ArrayList orders = new ArrayList();
    }

    [Author("Jane Programmer", Version = 2)]
    [IsTested]
    class Order
    {

    }

    class MainClass
    {
        private static bool IsMemberTested(MemberInfo member)
        {
            foreach (object attribute in member.GetCustomAttributes(true))
            {
                if (attribute is IsTestedAttribute)
                {
                    return true;
                }
            }
            return false;
        }

        private static void DumpAttributes(MemberInfo member)
        {
            Console.WriteLine("Attriutes for : " + member.Name);
            foreach (object attribute in member.GetCustomAttributes(true))
            {
                Console.WriteLine(attribute);
            }
        }

        public static void McMain(string[] args)
        {
            DumpAttributes(typeof(Account));

            foreach(MethodInfo method in (typeof(Account)).GetMethods())
            {
                if (IsMemberTested(method))
                {
                    Console.WriteLine("Member {0} is tested!", method.Name);
                }
                else
                {
                    Console.WriteLine("Member {0} is NOT tested!", method.Name);
                }
            }

            Console.WriteLine();

            DumpAttributes(typeof(Order));

            foreach (MethodInfo method in (typeof(Order)).GetMethods())
            {
                if (IsMemberTested(method))
                {
                    Console.WriteLine("Member {0} is tested!", method.Name);
                }
                else
                {
                    Console.WriteLine("Member {0} is NOT tested!", method.Name);
                }
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MainClass.McMain(args);
        }
    }
}
